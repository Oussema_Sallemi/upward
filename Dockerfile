FROM node:latest

WORKDIR /src

COPY . /src

RUN npm install

EXPOSE 8000

CMD npm start