const express = require("express");
const { middleware } = require("@magento/upward-js");
const dotenv = require("dotenv");

async function serve() {
  const app = express();
  const port = process.env.PORT || 8000;
  dotenv.config();

  app.get("/", (req, res) =>
    res.send("Hello from UPWARD Express Magento Middleware !")
  );

  const upwardMiddleware = await middleware(
    `${__dirname}/upward-middleware.yml`,
    {
      MAGENTO_GRAPHQL_URL: process.env.MAGENTO_GRAPHQL_URL,
      MAGENTO_REST_URL: process.env.MAGENTO_REST_URL,
    }
  );

  app.use(upwardMiddleware);

  app.listen(port, () => console.log(`Listening on port ${port}!`));
}

serve();
